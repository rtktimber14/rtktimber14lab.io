import { boot } from 'quasar/wrappers'
import { createI18n } from 'vue-i18n'
import messages from 'src/i18n'
import { TweenMax , TimelineMax } from "gsap";
import * as ScrollMagic from 'scrollmagic'
import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap";



export default boot(({ app }) => {
  ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);
  app.config.globalProperties.$TweenMax = TweenMax
  app.config.globalProperties.$TimelineMax = TimelineMax
  app.config.globalProperties.$ScrollMagic = ScrollMagic
  const i18n = createI18n({
    locale: 'en-GB',
    globalInjection: true,
    messages
  })

  // Set i18n instance on app
  app.use(i18n)
})
