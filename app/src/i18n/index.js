import enUS from './en-US'
import lvLV from './lv-LV'
import deDE from './de-DE'

export default {
  'en-GB': enUS,
  'lv': lvLV,
  'de': deDE
}
