export default {
  home: 'Mājas',
  about: 'Par mums',
  contact: 'Sazināties',
  title: 'JŪSU PARTNERIS AUGSTAS KVALITĀTES  KOKMATERIĀLIEM',
  desc: 'Mēs esam ilgtspējīgs kokmateriālu tirzdniecības uzņēmums, kas  nodrošina kvalitāti un stabilo sadarbību',
  title2: 'Par mums',
  desc2: 'Latvijas uzņēmums RTK TIMBER darbu uzsācis 2014. gadā. Uzņēmuma pamatnodarbošanās ir māju un koka konstrukciju celtniecībai izmantojamo zāģmateriālu, sagatavju palešu ražošanai, kā arī nestandarta koka izstrādājumu ražošana un tirdzniecība. <br/><br/> Mūsu birojs un noliktava atrodas Rīgas pilsētā, 5 km attālumā no Rīgas ostas. No Rīgas ostas varam nosūtīt produkciju uz jebkuru pasaules malu, izmantojot populārākās kuģniecības līnijas. Mēs arī operatīvi piegādājam preces mūsu Eiropas klientiem pa autoceļiem. Mēs vienmēr priecājamies par jauniem klientiem un garantējam, ka mēs saviem klientiem nodrošināsim kvalitatīvus produktus pēc iespējas īsākā laikā.',
  title3: 'Sazinies ar mums',
  email: 'E-pasts',
  tel: 'Tālr./Fax',
  desc3: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
}
