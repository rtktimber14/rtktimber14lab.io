export default {
  home: 'Home',
  about: 'About us',
  contact: 'Contacts',
  title: 'YOUR PARTNER FOR HIGH-QUALITY, SUSTAINABLE TIMBER',
  desc: 'We are a sustainable timber company, committed to quality, providing reliable solutions for all needs.',
  title2: 'About us',
  desc2: 'Our office and warehouse are located in the city of Riga, 5 km from the port of Riga, from which we can send products to any part of the world using the most popular shipping lines. <br/> We also promptly deliver goods to our European customers by road. We are always glad to new customers and are guaranteed to provide our customers with quality products in the shortest possible time.',
  title3: 'Contact',
  email: 'E-mail',
  tel: 'Tel./Fax',
  desc3: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
}
