export default {
  home: 'Heim',
  about: 'Über uns',
  contact: 'Kontakt',
  title: 'IHR PARTNER FÜR HOCHWERTIGES  HOLZ',
  desc: 'Wir sind ein nachhaltiges Holzunternehmen, das sich Qualität  und  langzeitige Partnerschaft bietet.',
  title2: 'Über uns',
  desc2: 'Das lettische Unternehmen RTK TIMBER ist im Jahr 2014 gegrūnded. Die Haupttätigkeit des Unternehmens ist die Produktion und der Verkauf von Schnittholz für den Bau von Häusern und Holzkonstruktionen, Zuschnitten für die Herstellung von Paletten sowie nicht standardmäßigen Holzprodukten.<br/>Unser Büro und Lager befinden sich in der Stadt Riga, 5 km vom Hafen von Riga entfernt, woraus können wir Produkte mit den gängigsten Reedereien in alle Teile der Welt versenden. Auch auf dem Landweg beliefern wir unsere europäischen Kunden zeitnah. Wir freuen uns immer über neue Kunden und garantieren unseren Kunden in kürzester Zeit Qualitätsprodukte zu liefern.',
  title3: 'Kontakt',
  email: 'E-mail',
  tel: 'Tel./Fax',
  desc3: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
}
